<?php

/**
 * @file
 * SearchApi processors for Addressfield fields.
 */

/**
 * Processor for converting a country code to a full country name
 * (e.g. "DE" => "Germany").
 */
class SearchApiAddressfieldCountryName extends SearchApiAbstractProcessor {

  protected function process(&$value) {
    // We don't touch integers, NULL values or the like.
    if (is_string($value) && !empty($value)) {
      require_once DRUPAL_ROOT . '/includes/locale.inc';
      $countries = country_get_list();
      if (isset($countries[$value])) {
        $value = $countries[$value];
      }
    }
  }

}
